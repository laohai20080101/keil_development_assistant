#include "parse_m51_from_keil.h"

#include <QFile>
#include <QDomDocument>
#include "find_path.h"
#include <QMessageBox>
#include <Qdebug>



parse_m51_from_keil::parse_m51_from_keil(QString path,uint64_t iram_max_size,uint64_t xram_max_size,
                                         uint64_t irom_max_size)
{

    iram_max=iram_max_size;
    xram_max=xram_max_size;
    irom_max=irom_max_size;


}

bool parse_m51_from_keil::to_parse_m51_from_keil(QString path)
{


    QString file_path=find_m51_path(path);
    qDebug()<<file_path;
    if(file_path.length()>3){

        parse_m51_file(file_path);
        return true;
    }
    else{
        qDebug()<<"路径错误,请检测输入的工程路径！";

        QMessageBox::critical(nullptr, "Error", "路径错误,请检测输入的工程路径！");


        return false;
    }
}

QString parse_m51_from_keil::find_m51_path(const QString &dir_path)
{


    QString m51_file_path;
    m51_file_path=find_path::find_file_path(dir_path,".m51");
    if(m51_file_path.length()<3)
        m51_file_path=find_path::find_file_path(dir_path,".M51");

    return m51_file_path;
}

bool parse_m51_from_keil::parse_m51_file(QString path)
{
    QFile file(path);

    iram=new use_percentage_bar(nullptr);
    xram=new use_percentage_bar(nullptr);
    irom=new use_percentage_bar(nullptr);


    iram->setName("iram");
    iram->setStorage(mc8051);
    iram->setBase_addr(0);
    xram->setName("xram");
    xram->setStorage(mc8051);
    xram->setBase_addr(0);
    xram->setMax_size(0);

    irom->setName("irom");
    irom->setStorage(mc8051);
    irom->setBase_addr(0);

    iram->setMax_size(iram_max*10.0);

    xram->setMax_size(xram_max*10.0);

    irom->setMax_size(irom_max*10.0);


    bool is_Image_component_sizes=false;
    bool is_Totals=false;

    //        info_list.clear();

    for(auto ram:ram_list)
    {
        delete  ram;
    }
    ram_list.clear();

    for(auto flash:flash_list)
    {
        delete  flash;
    }

    flash_list.clear();


    if (!file.open(QIODevice::ReadOnly))
    {
        qDebug()<<"open fail"<<endl;
        return false;
    }


    while (!file.atEnd())
    {
        QByteArray line = file.readLine();
        QString str(line);

        if(str.contains("Program Size"))
        {

            //                    qDebug() << str;
            //                parse_excution_region_info(str);

            QStringList str_list= str.split(" ");

            for(int i=0;i<str_list.length();i++)
            {
                //                    qDebug() << str_list[i];
                if(str_list[i].contains("xdata"))
                {
                    xram->setUsed_size((str_list[i].split("=")[1].toFloat())*10.0);
                    xram->setRemain_size(xram->getMax_size()-xram->getUsed_size());
                }else if(str_list[i].contains("XDATA"))
                {
                    xram->setUsed_size((str_list[i].split("=")[1].toFloat())*10.0);
                    xram->setRemain_size(xram->getMax_size()-xram->getUsed_size());
                }else if(str_list[i].contains("data"))
                {


                    iram->setUsed_size((str_list[i].split("=")[1].toFloat())*10.0);
                    iram->setRemain_size(iram->getMax_size()-iram->getUsed_size());
                }else if(str_list[i].contains("DATA"))
                {
                    iram->setUsed_size((str_list[i].split("=")[1].toFloat())*10.0);
                    iram->setRemain_size(iram->getMax_size()-iram->getUsed_size());
                }else if(str_list[i].contains("code"))
                {
                    irom->setUsed_size((str_list[i].split("=")[1].toFloat())*10.0);
                    irom->setRemain_size(irom->getMax_size()-irom->getUsed_size());
                }else if(str_list[i].contains("code"))
                {
                    irom->setUsed_size((str_list[i].split("=")[1].toFloat())*10.0);
                    irom->setRemain_size(irom->getMax_size()-irom->getUsed_size());
                }

            }


        }
        //            else if(str.contains("Image component sizes"))
        //            {
        //                is_Image_component_sizes=true;
        ////                       qDebug()<<str;
        //            }
    }



    file.close();


    ram_list.append(iram);
    if(xram->getMax_size()>0)
        ram_list.append(xram);
    flash_list.append(irom);

    qDebug()<<"ram:";
    for(auto ram:ram_list)
    {
        ram->print_bar_8051();
    }
    qDebug()<<"flash:";
    for(auto flash:flash_list)
    {
        flash->print_bar_8051();
    }


}
