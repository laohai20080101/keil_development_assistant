#ifndef PARSE_M51_FROM_KEIL_H
#define PARSE_M51_FR0M_KEIL_H

#include <QString>
#include "use_percentage_bar.h"
#include <QList>

class parse_m51_from_keil
{
public:
    parse_m51_from_keil(QString path,uint64_t iram_max_size,uint64_t xram_max_size,
                        uint64_t irom_max_size);

    uint64_t all_ram_size=0;
    uint64_t all_flash_size=0;

    uint64_t iram_max;
    uint64_t xram_max;
    uint64_t irom_max;
    use_percentage_bar* iram;
    use_percentage_bar* xram;
    use_percentage_bar* irom;
    QList<use_percentage_bar*>ram_list;
    QList<use_percentage_bar*>flash_list;


    bool to_parse_m51_from_keil(QString path);
    QString find_m51_path(const QString& dir_path);
    bool parse_m51_file(QString path);
};

#endif // PARSE_M51_FROM_KEIL_H
