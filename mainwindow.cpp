﻿#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QFileDialog>
#include "use_percentage_bar.h"

#include <Qdebug>
#include "flatui.h"
#include <QDebug>
#include <QDesktopServices>

#pragma execution_character_set("utf-8")
void MainWindow::winow_on_top(bool is_top)
{
    if(is_top)
        setWindowFlags(windowFlags()|Qt::WindowStaysOnTopHint);
    else
        setWindowFlags(windowFlags()&(~Qt::WindowStaysOnTopHint));
}



MainWindow::MainWindow(QWidget *parent) : FramelessMainWindow(parent), ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->initForm();
    winow_on_top(true);
    //    winow_on_top(false);
}

MainWindow::~MainWindow()
{
    delete cpu;
    delete map;
    delete ram_chart;
    delete flash_chart;
    delete ui;
}

void MainWindow::initForm()
{
    //设置标题栏控件
    ui->labTitle->setText("一个极客开源::keil开发助手");
    this->setWindowTitle(ui->labTitle->text());
    this->setTitleBar(ui->labTitle);

    //关联信号
    connect(this, SIGNAL(titleDblClick()), this, SLOT(titleDblClick()));
    connect(this, SIGNAL(windowStateChange(bool)), this, SLOT(windowStateChange(bool)));

    //设置样式表
    QStringList list;
    list << "#titleBar{background:#BBBBBB;}";
    list << "#titleBar{border-top-left-radius:8px;border-top-right-radius:8px;}";
    list << "#widgetMain{border:2px solid #BBBBBB;background:#FFFFFF;}";
    list << "#widgetMain{border-bottom-left-radius:8px;border-bottom-right-radius:8px;}";
    this->setStyleSheet(list.join(""));
    FlatUI::setScrollBarQss(ui->ram_scrollArea->verticalScrollBar(), 8, 120, 20, "#E5E5E5", "#D0D0D0", "#1ABC9C", "#E74C3C");
    FlatUI::setScrollBarQss(ui->flash_scrollArea->verticalScrollBar(), 8, 120, 20, "#E5E5E5", "#D0D0D0", "#1ABC9C", "#E74C3C");

    FlatUI::setScrollBarQss(ui->ram_table->horizontalScrollBar(), 8, 120, 20, "#E5E5E5", "#D0D0D0", "#1ABC9C", "#E74C3C");
    FlatUI::setScrollBarQss(ui->ram_table->verticalScrollBar(), 8, 120, 20, "#E5E5E5", "#D0D0D0", "#1ABC9C", "#E74C3C");
    FlatUI::setScrollBarQss(ui->flash_table->horizontalScrollBar(), 8, 120, 20, "#E5E5E5","#D0D0D0", "#1ABC9C", "#E74C3C");
    FlatUI::setScrollBarQss(ui->flash_table->verticalScrollBar(), 8, 120, 20, "#E5E5E5", "#D0D0D0", "#1ABC9C", "#E74C3C");

    FlatUI::setProgressQss(ui->ram_used_bar, 20,12,16, "#747976", "#1ABC9C");//#FAEC9C",#FFF5E7
    FlatUI::setProgressQss(ui->flash_used_bar, 20,12,16, "#747976", "#1ABC9C");

    main_window=this;


    //map_path="./";
    //project_path="./";

    ui->map_path->setText(map_path);
    ui->project_path->setText(project_path);

    ram_pieseries=new QPieSeries();
    flash_pieseries=new QPieSeries();



    ram_chart=new QChart();
    flash_chart=new QChart();





    //QChartView


    ram_chart->addSeries(ram_pieseries);
    ram_chart->setTitle("文件内存分布饼图");
    ram_chart->legend()->hide();
    // Set the theme to one of the predefined themes
    ram_chart->setTheme(QChart::ChartThemeQt);
    ram_chart->setBackgroundBrush(QBrush(Qt::gray));

    ram_chart->setTitleBrush(Qt::white);;  // 数据集分类标题文字颜色
    flash_chart->addSeries(flash_pieseries);
    flash_chart->setTitle("文件flash分布饼图");
    flash_chart->legend()->hide();
    // Set the theme to one of the predefined themes
    flash_chart->setTheme(QChart::ChartThemeQt);
    flash_chart->setBackgroundBrush(QBrush(Qt::gray));
    flash_chart->setTitleBrush(Qt::white);

    ui->ram_chart_view->setChart(ram_chart);
    ui->ram_chart_view->setRubberBand(QChartView::RectangleRubberBand);

    ui->flash_chart_view->setChart(flash_chart);
    ui->flash_chart_view->setRubberBand(QChartView::RectangleRubberBand);


    if(map_path.length()>1&&project_path.length()>1)
    {
        cpu= new parse_keil_project(ui->project_path->text());
        if(cpu->to_parse_keil_project(ui->project_path->text()))
        {
            update_cpu_info();





            if(cpu->getCpu_core_name()!="8051")
            {

            }else {

            }

        }
        if(cpu->getCpu_core_name()!="8051")
        {
            map= new parse_map_from_keil( ui->map_path->text(),cpu->IRAM.getBase_addr(),cpu->IRAM.getMax_size(),cpu->IROM.getBase_addr(),cpu->IROM.getMax_size());
            if(map->to_parse_map_from_keil(ui->map_path->text()))
            {
                map_calculate_size_by_address(map);
                update_map_info();
            }
        }else {

            m51 =new parse_m51_from_keil(ui->map_path->text(),cpu->IRAM.getMax_size(),cpu->XRAM.getMax_size(),cpu->IROM.getMax_size());
            if(m51->to_parse_m51_from_keil(ui->map_path->text()))
            {
                update_m51_info();
            }

        }
    }

    fflush(stdout);

}



void MainWindow::map_calculate_size_by_address(parse_map_from_keil* map)
{


    for(auto ram:map->ram_list)
    {
        if(ram->getMax_size()>100*1024*1024)
        {

            uint64_t recent_base=cpu->IRAM.getBase_addr()+cpu->IRAM.getMax_size();

            for(auto recent_ram :map->ram_list)
            {
                if(recent_ram->getBase_addr()>ram->getBase_addr())
                {
                    if(recent_ram->getBase_addr()-ram->getBase_addr()<recent_base-ram->getBase_addr())
                        recent_base=recent_ram->getBase_addr();
                }

            }

            ram->setMax_size(recent_base-ram->getBase_addr());
        }
    }
    for(auto flash:map->flash_list)
    {
        if(flash->getMax_size()>100*1024*1024)
        {

            uint64_t recent_base=cpu->IROM.getBase_addr()+cpu->IROM.getMax_size();
            for(auto recent_flash :map->flash_list)
            {

                if(recent_flash->getBase_addr()>flash->getBase_addr())
                {

                    if(recent_flash->getBase_addr()-flash->getBase_addr()<recent_base-flash->getBase_addr())
                        recent_base=recent_flash->getBase_addr();

                }
            }
            flash->setMax_size(recent_base-flash->getBase_addr());
        }
    }

}

void MainWindow::on_btn_set_project_path_clicked()
{
    //获取选择的目录路径
    QString selectedDir=QFileDialog::getExistingDirectory(this,"选择一个目录","./",QFileDialog::ShowDirsOnly);
    //若目录路径不为空
    if (!selectedDir.isEmpty())
    {
        //将路径中的斜杠替换为反斜杠
        selectedDir = selectedDir.replace(QRegExp("\\"), "/");
        //显示选择的目录路径
        ui->project_path->setText(selectedDir);
        ui->map_path->setText(selectedDir);
    }

}


void MainWindow::set_bar_style(QProgressBar *bar,float per)
{
    if( per/1.0>0.85)
    {
        FlatUI::setProgressQss(bar, 20,12,16, "#747976", "#E74C3C");

    }else if( per>0.75)
    {
        FlatUI::setProgressQss(bar, 20,12,16, "#747976", "#F9D927");

    }
    else
    {
        FlatUI::setProgressQss(bar, 20,12,16, "#747976", "#1ABC9C");
    }

}

void MainWindow::on_btn_set_map_path_clicked()
{
    //获取选择的目录路径
    QString selectedDir=QFileDialog::getExistingDirectory(this,"选择一个目录","./",QFileDialog::ShowDirsOnly);
    //若目录路径不为空
    if (!selectedDir.isEmpty())
    {
        //将路径中的斜杠替换为反斜杠
        selectedDir = selectedDir.replace(QRegExp("\\"), "/");
        //显示选择的目录路径
        ui->map_path->setText(selectedDir);
    }
}

void MainWindow::on_update_info_clicked()
{


    if(cpu!=nullptr)
    {
        delete  cpu;
        cpu=nullptr;
    }
    cpu= new parse_keil_project(ui->project_path->text());
    if(cpu->to_parse_keil_project(ui->project_path->text()))
        update_cpu_info();

    if(cpu->getCpu_core_name()!="8051")
    {

        if(map!=nullptr)
        {
            delete  map;
            map=nullptr;
        }
        map= new parse_map_from_keil(ui->map_path->text(),cpu->IRAM.getBase_addr(),cpu->IRAM.getMax_size(),cpu->IROM.getBase_addr(),cpu->IROM.getMax_size());
        if(map->to_parse_map_from_keil(ui->map_path->text()))
        {
            map_calculate_size_by_address(map);

            update_map_info();
        }
    }else {



        if(m51!=nullptr)
        {
            delete  m51;
            m51=nullptr;
        }
        m51 =new parse_m51_from_keil(ui->map_path->text(),cpu->IRAM.getMax_size(),cpu->XRAM.getMax_size(),cpu->IROM.getMax_size());

        if(m51->to_parse_m51_from_keil(ui->map_path->text()))
            update_m51_info();

    }

    fflush(stdout);


}

void MainWindow::on_ram_table_itemSelectionChanged()
{
    QList<QTableWidgetItem*> selectedItems = ui->ram_table->selectedItems();


    // Reset all pie slices to not exploded (not highlighted)
    foreach (QPieSlice* slice, ram_pieseries->slices()) {
        slice->setExploded(false);
        slice->setLabelVisible(false);

    }
    foreach (QTableWidgetItem* item, selectedItems) {
        int row = item->row();
        //           qDebug() << row;

        QPieSlice *slice =  ram_pieseries->slices().at(row);
        slice->setExploded();//突出这一块
        slice->setLabelVisible();//设置这一块可见数据
        slice->setLabelBrush(Qt::white);

    }

}

void MainWindow::on_flash_table_itemSelectionChanged()
{
    QList<QTableWidgetItem*> selectedItems = ui->flash_table->selectedItems();


    // Reset all pie slices to not exploded (not highlighted)
    foreach (QPieSlice* slice, flash_pieseries->slices()) {
        slice->setExploded(false);
        slice->setLabelVisible(false);

    }
    foreach (QTableWidgetItem* item, selectedItems) {
        int row = item->row();
        //           qDebug() << row;

        QPieSlice *slice =  flash_pieseries->slices().at(row);
        slice->setExploded();//突出这一块
        slice->setLabelVisible();//设置这一块可见数据
        slice->setLabelBrush(Qt::white);


    }
}



void MainWindow::update_cpu_info(void)
{
    ui->cpu_name->setTitle(cpu->getCpu_name());
    ui->cpu_core->setText("单片机核心:"+cpu->getCpu_core_name());
    ui->cpu_colck->setText(QString().sprintf("最高主频:%.2f MHz",cpu->getCpu_max_clock()/1000000.0));
    ui->iram->setText(QString().sprintf("内部ram 地址:%#x 容量：%.2f KB",cpu->IRAM.getBase_addr(),cpu->IRAM.getMax_size()/1024.0));
    ui->flash->setText(QString().sprintf("内部flash 地址:%#x 容量：%.2f KB",cpu->IROM.getBase_addr(),cpu->IROM.getMax_size()/1024.0));

}



void MainWindow::clearLayout(QLayout* layout) {
    QLayoutItem* item;
    while ((item = layout->takeAt(0))) {
        QWidget* widget = item->widget();
        if (widget) {
            layout->removeWidget(widget); // 将小部件从布局中移除
            widget->setParent(nullptr); // 设置父对象为nullptr，避免悬挂指针问题
            delete widget; // 删除小部件并释放内存
        }
        delete item; // 删除布局项
    }
}


void MainWindow::update_m51_info(void)
{
    uint64_t ram_all_max_size=0;
    uint64_t ram_all_used_size=0;
    uint64_t ram_min_base_addr=m51->ram_list.at(0)->getBase_addr();

    uint64_t flash_all_max_size=0;
    uint64_t flash_all_used_size=0;
    uint64_t flash_min_base_addr=m51->flash_list.at(0)->getBase_addr();
    uint16_t i;


    clearLayout(ui->ram_scrollAreaWidgetContents->layout());

    // 移除并释放布局中的所有小部件

    clearLayout(ui->flash_scrollAreaWidgetContents->layout());

    // 移除并释放布局中的所有小部件
    for(uint64_t row_cnt=0;row_cnt<ui->ram_table->rowCount();row_cnt++)
    {

        ui->ram_table->removeRow(row_cnt);
    }
    ui->ram_table->clear();

    ram_pieseries->clear();

    for(uint64_t row_cnt=0;row_cnt<ui->flash_table->rowCount();row_cnt++)
    {
        ui->flash_table->removeRow(row_cnt);
    }
    ui->flash_table->clear();
    flash_pieseries->clear();

    for(i=0;i<m51->ram_list.size();i++)
    {

        ram_all_used_size+=m51->ram_list.at(i)->getUsed_size();
        ram_all_max_size+=m51->ram_list.at(i)->getMax_size();
        if(ram_min_base_addr>m51->ram_list.at(0)->getBase_addr())
            ram_min_base_addr=m51->ram_list.at(i)->getBase_addr();
        ui->ram_scrollAreaWidgetContents->layout()->addWidget(m51->ram_list.at(i));
    }



    ui->ram_addr->setText(QString().sprintf("地址:%#x",ram_min_base_addr));
    ui->ram_used_bar->setRange(0,ram_all_max_size);
    ui->ram_used_bar->setValue(ram_all_used_size);
    ui->ram_remain->setText(QString().sprintf("剩余:%.2f B",ram_all_max_size/10.0-ram_all_used_size/10.0));
    if(ram_all_max_size/10.0<=1024)
    {
        ui->ram_used->setText(QString().sprintf("占用:%.2f B / (%.2f B)",ram_all_used_size/10.0,ram_all_max_size/10.0));
    }else
    {
        ui->ram_used->setText(QString().sprintf("占用:%.2f B / (%.2f KB)",ram_all_used_size/1024.0,ram_all_max_size/10.0/1024.0));
    }

    set_bar_style(ui->ram_used_bar,ram_all_used_size/1.0/ram_all_max_size/1.0);

    for(i=0;i<m51->flash_list.size();i++)
    {
        flash_all_used_size+=m51->flash_list.at(i)->getUsed_size();
        flash_all_max_size+=m51->flash_list.at(i)->getMax_size();
        if(flash_min_base_addr>m51->flash_list.at(0)->getBase_addr())
            flash_min_base_addr=m51->flash_list.at(i)->getBase_addr();
        ui->flash_scrollAreaWidgetContents->layout()->addWidget(m51->flash_list.at(i));
    }

    ui->flash_addr->setText(QString().sprintf("地址:%#x",flash_min_base_addr));
    ui->flash_used_bar->setRange(0,flash_all_max_size);
    ui->flash_used_bar->setValue(flash_all_used_size);
    ui->flash_remain->setText(QString().sprintf("剩余:%.2f B",flash_all_max_size/10.0-flash_all_used_size/10.0));
    if(flash_all_max_size/10.0<=1024)
    {
        ui->flash_used->setText(QString().sprintf("占用:%.2f B / (%.2f B)",flash_all_used_size/10.0,flash_all_max_size/10.0));
    }else
    {
        ui->flash_used->setText(QString().sprintf("占用:%.2f B / (%.2f KB)",flash_all_used_size/1024.0,flash_all_max_size/10.0/1024.0));
    }
    set_bar_style(ui->flash_used_bar,ram_all_used_size/1.0/ram_all_max_size/1.0);

    if(ui->ram_scrollArea->verticalScrollBar()!=nullptr)
        FlatUI::setScrollBarQss(ui->ram_scrollArea->verticalScrollBar(), 8, 120, 20, "##E5E5E5", "#D0D0D0", "#1ABC9C", "#E74C3C");
    if(ui->ram_scrollArea->verticalScrollBar()!=nullptr)
        FlatUI::setScrollBarQss(ui->flash_scrollArea->verticalScrollBar(), 8, 120, 20, "#E5E5E5", "#D0D0D0", "#1ABC9C", "#E74C3C");


}

void MainWindow::setPieSliceStyle(QPieSlice* slice, qreal percentage, qreal maxPercentage)
{

    // Define an array of custom green colors to represent different percentage ranges
    QColor colors[] = {
        QColor("#D1FF4D"), // Light Green Yellow
        QColor("#C0FF3E"), // Greenish Yellow
        QColor("#ADFF2F"), // Green Yellow
        QColor("#7CFC00"), // Lawn Green
        QColor("#00FF00"), // Lime Green
        QColor("#008000"), // Green
        QColor("#006400"), // Dark Green
    };

    // Calculate the relative percentage compared to the maxPercentage
    qreal relativePercentage = (percentage / maxPercentage) * 100.0;

    // Determine the index of the color based on relative percentage
    int colorIndex = qMin(int(relativePercentage) / (100 / (sizeof(colors) / sizeof(colors[0]))), sizeof(colors) / sizeof(colors[0]) - 1);

    // Set the color of the slice based on the color index
    slice->setColor(colors[colorIndex]);


}

void MainWindow::update_map_info(void)
{

    if(map->ram_list.length()==0)
        return;

    if(map->flash_list.length()==0)
        return;

    uint64_t ram_all_max_size=0;
    uint64_t ram_all_used_size=0;
    uint64_t ram_min_base_addr=map->ram_list.at(0)->getBase_addr();

    uint64_t flash_all_max_size=0;
    uint64_t flash_all_used_size=0;
    uint64_t flash_min_base_addr=map->flash_list.at(0)->getBase_addr();
    uint16_t i;


    clearLayout(ui->ram_scrollAreaWidgetContents->layout());
    //    // 移除并释放布局中的所有小部件
    clearLayout(ui->flash_scrollAreaWidgetContents->layout());
    //    // 移除并释放布局中的所有小部件
    for(uint64_t row_cnt=0;row_cnt<ui->ram_table->rowCount();row_cnt++)
    {

        ui->ram_table->removeRow(row_cnt);
    }
    ui->ram_table->clear();
    ram_pieseries->clear();

    for(uint64_t row_cnt=0;row_cnt<ui->flash_table->rowCount();row_cnt++)
    {
        ui->flash_table->removeRow(row_cnt);
    }
    ui->flash_table->clear();
    flash_pieseries->clear();



    for(i=0;i<map->ram_list.size();i++)
    {

        ram_all_used_size+=map->ram_list.at(i)->getUsed_size();
        ram_all_max_size+=map->ram_list.at(i)->getMax_size();
        if(ram_min_base_addr>map->ram_list.at(0)->getBase_addr())
            ram_min_base_addr=map->ram_list.at(i)->getBase_addr();
        ui->ram_scrollAreaWidgetContents->layout()->addWidget(map->ram_list.at(i));
    }



    ui->ram_addr->setText(QString().sprintf("地址:%#x",ram_min_base_addr));
    ui->ram_used_bar->setRange(0,ram_all_max_size);
    ui->ram_used_bar->setValue(ram_all_used_size);
    ui->ram_remain->setText(QString().sprintf("剩余:%llu B",ram_all_max_size-ram_all_used_size));
    if(ram_all_max_size<=1024*1024)
    {
        ui->ram_used->setText(QString().sprintf("占用:%.2f KB / (%.2f KB)",ram_all_used_size/1024.0,ram_all_max_size/1024.0));
    }else
    {
        ui->ram_used->setText(QString().sprintf("占用:%.4f MB / (%.4f MB)",ram_all_used_size/1024.0/1024.0,ram_all_max_size/1024.0/1024.0));
    }

    set_bar_style(ui->ram_used_bar,ram_all_used_size/1.0/ram_all_max_size/1.0);


    for(i=0;i<map->flash_list.size();i++)
    {
        flash_all_used_size+=map->flash_list.at(i)->getUsed_size();
        flash_all_max_size+=map->flash_list.at(i)->getMax_size();
        if(flash_min_base_addr>map->flash_list.at(0)->getBase_addr())
        flash_min_base_addr=map->flash_list.at(i)->getBase_addr();
        ui->flash_scrollAreaWidgetContents->layout()->addWidget(map->flash_list.at(i));
    }

    ui->flash_addr->setText(QString().sprintf("地址:%#x",flash_min_base_addr));
    ui->flash_used_bar->setRange(0,flash_all_max_size);
    ui->flash_used_bar->setValue(flash_all_used_size);
    ui->flash_remain->setText(QString().sprintf("剩余:%llu B",flash_all_max_size-flash_all_used_size));
    if(flash_all_max_size<=1024*1024)
    {
        ui->flash_used->setText(QString().sprintf("占用:%.2f KB / (%.2f KB)",flash_all_used_size/1024.0,flash_all_max_size/1024.0));
    }else
    {
        ui->flash_used->setText(QString().sprintf("占用:%.4f MB / (%.4f MB)",flash_all_used_size/1024.0/1024.0,flash_all_max_size/1024.0/1024.0));
    }

    set_bar_style(ui->flash_used_bar,flash_all_used_size/1.0/flash_all_max_size/1.0);




    QList <image_item_t*> info_lsit=  map->getInfo_list();
    uint64_t all_ram_size = map->getAll_ram_size();
    uint64_t  all_flash_size =map->getAll_flash_size();
    uint64_t ram_row=0;
    uint64_t flash_row=0;
    for(auto item:info_lsit)
    {
        if(item->RW_Data.toUInt()+item->ZI_Data.toUInt()!=0)
        {
            writeTableData(ram_table,ram_row,0,item->Object_Name);
            writeTableData(ram_table,ram_row,1,QString::number(item->RW_Data.toUInt()+item->ZI_Data.toUInt()));
            writeTableData(ram_table,ram_row,3,item->RW_Data);
            writeTableData(ram_table,ram_row,4,item->ZI_Data);
            all_ram_size+=item->RW_Data.toUInt()+item->ZI_Data.toUInt();
            ram_row++;
        }

        if(item->Code.toUInt()+item->RO_Data.toUInt()+item->RW_Data.toUInt()!=0)
        {


            writeTableData(flash_table,flash_row,0,item->Object_Name);
            writeTableData(flash_table,flash_row,1,QString::number(item->Code.toUInt()+item->RO_Data.toUInt()+item->RW_Data.toUInt()));
            writeTableData(flash_table,flash_row,3,item->Code);
            writeTableData(flash_table,flash_row,4,item->RO_Data);
            writeTableData(flash_table,flash_row,5,item->RW_Data);
            all_flash_size+=item->Code.toUInt()+item->RO_Data.toUInt()+item->RW_Data.toUInt();
            flash_row++;
        }
    }







    ui->ram_per->setTitle(QString().sprintf("used_ram=RW+ZI=%llu",all_ram_size));
    ui->flash_per->setTitle(QString().sprintf("used_flash=RW+Code+RO=%llu",all_flash_size));





    for(uint64_t row_cnt=0;row_cnt<ui->ram_table->rowCount();row_cnt++)
    {
        writeTableData(ram_table,row_cnt,2,QString().sprintf("%05.2f%%",ui->ram_table->item(row_cnt,1)->text().toUInt()*100.0/all_ram_size/1.0));
    }

    for(uint64_t row_cnt=0;row_cnt<ui->flash_table->rowCount();row_cnt++)
    {
        writeTableData(flash_table,row_cnt,2,QString().sprintf("%05.2f%%",ui->flash_table->item(row_cnt,1)->text().toUInt()*100.0/all_flash_size/1.0));
    }

    ui->ram_table->sortByColumn(2,Qt::SortOrder::DescendingOrder);
    ui->flash_table->sortByColumn(2,Qt::SortOrder::DescendingOrder);



    for(uint64_t row_cnt=0;row_cnt<ui->ram_table->rowCount();row_cnt++)
    {




        QPieSlice *slice =ram_pieseries->append(ui->ram_table->item(row_cnt,0)->text()+":"+ui->ram_table->item(row_cnt,2)->text(),ui->ram_table->item(row_cnt,1)->text().toUInt());

        setPieSliceStyle(slice,ui->ram_table->item(row_cnt,2)->text().split("%")[0].toFloat(),ui->ram_table->item(0,2)->text().split("%")[0].toFloat());



    }

    for(uint64_t row_cnt=0;row_cnt<ui->flash_table->rowCount();row_cnt++)
    {
        QPieSlice *slice =flash_pieseries->append(ui->flash_table->item(row_cnt,0)->text()+":"+ui->flash_table->item(row_cnt,2)->text(),ui->flash_table->item(row_cnt,1)->text().toUInt());
        setPieSliceStyle(slice,ui->flash_table->item(row_cnt,2)->text().split("%")[0].toFloat(),ui->flash_table->item(0,2)->text().split("%")[0].toFloat());

    }


    if(ui->ram_scrollArea->verticalScrollBar()!=nullptr)
        FlatUI::setScrollBarQss(ui->ram_scrollArea->verticalScrollBar(), 8, 120, 20, "#E5E5E5", "#D0D0D0", "#1ABC9C", "#E74C3C");
    if(ui->ram_scrollArea->verticalScrollBar()!=nullptr)
        FlatUI::setScrollBarQss(ui->flash_scrollArea->verticalScrollBar(), 8, 120, 20, "#E5E5E5", "#D0D0D0", "#1ABC9C", "#E74C3C");


}










QString MainWindow::readTableData(ui_table_t table,int row, int column)
{
    QString data;
    QTableWidget*  tableWidget ;
    if( table==ram_table)tableWidget=ui->ram_table;
    else tableWidget=ui->flash_table;

    if (row < tableWidget->rowCount() && column <tableWidget->columnCount()) {
        QTableWidgetItem *item =tableWidget->item(row, column);
        if (item) {
            data = item->text();
            //              qDebug() << "Data at (" << row << ", " << column << "): " << data;
        }
    }
    return data;
}


void MainWindow::writeTableData(ui_table_t table,int row, int column, const QString &data)
{


    QTableWidget* tableWidget=nullptr;
    if (table == ram_table)
    {

        tableWidget=ui->ram_table;

    }
    else if (table == flash_table)
    {
        tableWidget = ui->flash_table;

    }
    else
    {
        qDebug() << "Invalid table specified.";
        return;
    }


    if (row >= tableWidget->rowCount())
    {
        tableWidget->setRowCount(row + 1);
    }
    if (column >= tableWidget->columnCount())
    {
        tableWidget->setColumnCount(column + 1);
    }

    if (!tableWidget->item(row, column))
    {
        QTableWidgetItem *newItem = new QTableWidgetItem(data);
        tableWidget->setItem(row, column, newItem);
    }
    else
    {
        tableWidget->item(row, column)->setText(data);
    }




}




























void MainWindow::titleDblClick()
{
    on_btnMenu_Max_clicked();
}

void MainWindow::windowStateChange(bool max)
{
    ui->btnMenu_Max->setText(max ? "还原" : "最大");
}

void MainWindow::on_btnMenu_Min_clicked()
{
#ifdef Q_OS_MACOS
    this->setWindowFlags(this->windowFlags() & ~Qt::FramelessWindowHint);
#endif
    this->showMinimized();
}

void MainWindow::on_btnMenu_Max_clicked()
{
    if (this->isMaximized()) {
        this->showNormal();
        ui->btnMenu_Max->setText("最大");
    } else {
        this->showMaximized();
        ui->btnMenu_Max->setText("还原");
    }
}

void MainWindow::on_btnMenu_Close_clicked()
{
    this->close();
}

void MainWindow::on_gitee_index_clicked()
{
    QDesktopServices::openUrl(QUrl("https://gitee.com/nikolan"));

}

void MainWindow::on_icon_clicked()
{
    QDesktopServices::openUrl(QUrl("https://gitee.com/nikolan/keil_development_assistant"));
}
