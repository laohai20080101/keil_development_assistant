#include "mainwindow.h"
#include <QApplication>

#include <QSharedMemory>
#include <QMessageBox>


#include <QString>
#include <QTextCodec>

MainWindow* main_window=nullptr;

#include "only_run_by_file.h"
void  onAppExit()
{
    // 删除 run 文件
    QFile::remove("run");
}

QString project_path;
QString map_path;




QString gb2312ToQString(const char *gb2312Data) {
    QTextCodec *gb2312Codec = QTextCodec::codecForName("GB2312");
    if (!gb2312Codec) {
        qWarning("GB2312 codec not available. Cannot convert to QString.");
        return QString();
    }

    QByteArray byteArray = QByteArray::fromRawData(gb2312Data, strlen(gb2312Data));
    return gb2312Codec->toUnicode(byteArray);
}




//QString executeCmdCommand(const QString& cmdCommand) {
//    QProcess process;

//    // Set the command to be executed (cmd in this case)
//    QString command = "cmd.exe";

//    // Set the arguments to pass to the command
//    QStringList arguments;
//    arguments << "/c" << cmdCommand;

//    // Start the process
//    process.start(command, arguments);

//    // Wait for the process to finish
//    process.waitForFinished();

//    // Read the output from the process
//    QString output = process.readAllStandardOutput();

//    return output;

//}

int main(int argc, char *argv[])
{



    // Execute the cmd command and get the output
    //    QString result = executeCmdCommand("cd ");

    // Display the output
    //    qDebug() << "Output:" << result;


    for(int i=0;i<argc;i++)
        qDebug() <<argv[i];

    if(argc==3)
    {

        project_path=gb2312ToQString(argv[1]);
        map_path=gb2312ToQString(argv[2]);

        project_path.replace("\"", "");
        map_path.replace("\"", "");

        project_path.replace("“", "");
        project_path.replace("”", "");
        map_path.replace("“", "");
        map_path.replace("”", "");

    }else if(argc==2)
    {
        project_path=map_path=gb2312ToQString(argv[1]);
        project_path.replace("\"", "");
        map_path.replace("\"", "");
        project_path.replace("“", "");
        project_path.replace("”", "");
        map_path.replace("“", "");
        map_path.replace("”", "");

    }else

    {
        project_path=map_path="";
    }


    QApplication a(argc, argv);


    //    only_run_by_file programController;
    //       QObject::connect(&programController, &only_run_by_file::runFileDeleted, &a, &QCoreApplication::quit);
    // 注册程序退出的处理函数
    QObject::connect(&a, &QCoreApplication::aboutToQuit, onAppExit);

    MainWindow w;

    w.show();

    return a.exec();
}
