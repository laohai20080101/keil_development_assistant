#include "parse_keil_project.h"


#include <QFile>
#include <QDomDocument>
#include <Qtxml>
#include <QDebug>
#include <qxmlstream.h>


#include "find_path.h"


QString parse_keil_project::find_project_path(const QString& dir_path)
{

    return find_path::find_file_path(dir_path,".uvproj");

}

parse_keil_project::parse_keil_project(QString path)
{



    IRAM.setName("IRAM");
    XRAM.setName("XRAM");
    IROM.setName("IROM");

    //   to_parse_keil_project(path);



}


bool parse_keil_project::to_parse_keil_project(QString path)
{
    project_path=path;
    QString file_path;

    if(path.contains(".uvproj"))
    {
        parse_project_file(path);
        return true;
    }

    file_path=find_project_path(project_path);
    qDebug()<<file_path;
    if(file_path.length()<3)
    {
        return false;
    }
    else
    {
        parse_project_file(file_path);
        return true;
    }
}

bool parse_keil_project::parse_info_item_max_clock(const QString& info_item)
{

    QStringList info_list;
    uint64_t data;
    bool is_int;
    uint8_t data_cnt=0;
    info_list= info_item.split("(");//按"\""分割
    cpu_max_clock=info_list[1].toInt(&is_int,10);


}

QString parse_keil_project::getCpu_core_name() const
{
    return cpu_core_name;
}

uint64_t parse_keil_project::getCpu_max_clock() const
{
    return cpu_max_clock;
}

QString parse_keil_project::getProject_path() const
{
    return project_path;
}

void parse_keil_project::setProject_path(const QString &value)
{
    project_path = value;
}

QString parse_keil_project::getCpu_name() const
{
    return cpu_name;
}





bool parse_keil_project::parse_info_item_core_name(const QString& info_item)
{

    QStringList info_list;
    uint64_t data;
    bool is_int;
    uint8_t data_cnt=0;
    info_list= info_item.split("\"");//按"\""分割
    cpu_core_name=info_list[1];

}

bool parse_keil_project::parse_info_item_storage(const QString& info_item,use_percentage_bar& storage )
{
    QStringList info_list;
    uint64_t data;
    bool is_int;
    uint8_t data_cnt=0;
    QString sp_char=",";

    if(info_item.contains("-"))
        sp_char="-";

    info_list= info_item.split("(");//按"("分割
    info_list= info_list[1].split(sp_char);//按","或者"-"分割

    data_cnt==0;
    for(auto info:info_list)
    {
        info.remove(QChar(','), Qt::CaseInsensitive);//去除","
        info.remove(QChar(')'), Qt::CaseInsensitive);//去除")"
        info.remove(QChar('x'), Qt::CaseInsensitive);//去除"0x"的'x'
        data=info.toInt(&is_int,16);//转16进制

        if(is_int)//判断是否是16进制整数
        {
            if(data_cnt==0)//第一个16进制数是base地址
            {
                storage.setBase_addr(data);
                data_cnt++;

            }else//第二个十六进制数是 max容量大小
            {
                storage.setMax_size(data);
                data_cnt=0;


            }


        }



    }


    qDebug()<<QString().sprintf("%s base:%#x size:%.2f KB",storage.getName().toStdString().data(), storage.getBase_addr(),storage.getMax_size()/1024.0);

}

bool parse_keil_project::parse_cpu_info(const QString& cpu_info)
{

    QStringList cpu_info_list = cpu_info.split(")");


    for(auto info_item:cpu_info_list)
    {
        //        qDebug()<<info_item;
        if(info_item.contains("IRAM"))//包含"IRAM"字段
        {
            parse_info_item_storage(info_item,IRAM);
        }
        else if(info_item.contains("XRAM"))//包含"XRAM"字段
        {
            parse_info_item_storage(info_item,XRAM);
        }
        else if(info_item.contains("IROM"))
        {
            parse_info_item_storage(info_item,IROM);
        }
        else if(info_item.contains("CPUTYPE"))
        {
            parse_info_item_core_name(info_item);
            qDebug()<<"core:"<<cpu_core_name;
        }
        else if(info_item.contains("CLOCK"))
        {
            parse_info_item_max_clock(info_item);
            qDebug()<<"main_clock:"<<cpu_max_clock<<"MHz";
        }




    }




}

bool parse_keil_project::parse_project_file(QString path)
{
    QFile file(path);
    if (!file.open(QIODevice::ReadOnly))
    {
        qDebug()<<"open fail"<<endl;
        return 0;
    }

    QXmlStreamReader reader(&file);
    while(!reader.atEnd())
    {

        if(reader.isStartElement())
        {
            //                        qDebug()<<"<"<<reader.name();

            if("Cpu"==reader.name())
            {

                parse_cpu_info(reader.readElementText());

            }else if("Device"==reader.name())
            {

                cpu_name=(reader.readElementText());

            }else if("ToolsetName" ==reader.name())
            {

                if(reader.readElementText().contains("MCS-51"))
                    cpu_core_name="8051";

            }



        }
        if(reader.isEndElement())
        {
            //                        qDebug()<<">"<<reader.name();
        }

        reader.readNext();


    }
    if (reader.hasError())
    {
        qDebug()<<reader.errorString();
    }

    file.close();

}
