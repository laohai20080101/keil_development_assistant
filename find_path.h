#ifndef FIND_PATH_H
#define FIND_PATH_H

#include <QString>
class find_path
{
public:
    find_path();
    static QString find_file_path(const QString& dir_path,const QString& key_world);

};

#endif // FIND_PATH_H
