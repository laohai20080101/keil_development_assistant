#include "find_path.h"
#include <QFile>
#include <QDebug>
#include <QDomDocument>
#include <QFileInfoList>
#include <QDir>

find_path::find_path()
{

}
QString find_path::find_file_path(const QString& dir_path,const QString& key_world)
{
    QFileInfoList fileInfoList;
    QString file_path="";
    QDir dir(dir_path);
    foreach(QFileInfo info,dir.entryInfoList(QDir::NoDotAndDotDot | QDir::Dirs)){

        //             qDebug()<<"dir:"<<info.filePath();

        file_path=find_file_path(info.filePath(),key_world);
        if(file_path!="")
            return file_path;

    }

    foreach(QFileInfo info,dir.entryInfoList(QDir::Files)){
        fileInfoList.append(info);
        if(info.fileName().contains(key_world))
        {


            file_path=info.path()+"/"+info.fileName();
            //                qDebug()<<info.path()+"/"+info.fileName();
            return file_path;
        }

    }
    return file_path;
}
