#ifndef ONLY_RUN_BY_FILE_H
#define ONLY_RUN_BY_FILE_H

#include <QObject>
#include <QFile>
#include <QTimer>

class only_run_by_file : public QObject
{
    Q_OBJECT
public:
    explicit only_run_by_file(QObject *parent = nullptr);

private:
    const QString runFileName;
    const QString closeFileName;

    void createCloseFile();
    void createRunFile();
    void deleteCloseFile();
    void deleteRunFile();
    QTimer* closeTimer;
    QTimer* timer ;
signals:
    void runFileDeleted();
    void closeFileDetected();

public slots:
    void checkRunFile();
    void checkCloseFile();
};
#endif // ONLY_RUN_BY_FILE_H
