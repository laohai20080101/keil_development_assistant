#ifndef PARSE_KEIL_PROJECT_H
#define PARSE_KEIL_PROJECT_H

#include <QString>
#include <use_percentage_bar.h>


class parse_keil_project
{

public:

    parse_keil_project(QString path);
    bool to_parse_keil_project(QString path);
    QString find_project_path(const QString& dir_path);
    bool parse_project_file(QString path);
    bool parse_cpu_info(const QString& cpu_info);

    bool parse_info_item_storage(const QString& info_item,use_percentage_bar& storage );
    bool parse_info_item_core_name(const QString& info_item);
    bool parse_info_item_max_clock(const QString& info_item);

    QString getCpu_core_name() const;

    uint64_t getCpu_max_clock() const;

    QString getProject_path() const;
    void setProject_path(const QString &value);

    use_percentage_bar IRAM;
    use_percentage_bar IROM;
    use_percentage_bar XRAM;


    QString getCpu_name() const;

private:
    QString project_path;
    //    uint64_t iram_base;
    //    uint64_t xram_base;
    //    uint64_t irom_base;
    //    uint64_t iram_size;
    //    uint64_t xram_size;
    //    uint64_t irom_size;
    QString cpu_name;
    QString cpu_core_name;
    uint64_t cpu_max_clock;

};

#endif // PARSE_KEIL_PROJECT_H
