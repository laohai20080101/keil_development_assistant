#include "use_percentage_bar.h"
#include "ui_use_percentage_bar.h"
#include <QDebug>
#include <QString>
#include <flatui.h>

use_percentage_bar::use_percentage_bar(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::use_percentage_bar)
{
    ui->setupUi(this);

    FlatUI::setProgressQss(ui->used_bar, 20,12,16, "#747976", "#1ABC9C");

}
void use_percentage_bar::set_bar_style(QProgressBar *bar,float per)
{
    if( per/1.0>0.85)
    {
        FlatUI::setProgressQss(bar, 20,12,16, "#747976", "#E74C3C");

    }else if( per>0.75)
    {
        FlatUI::setProgressQss(bar, 20,12,16, "#747976", "#F9D927");

    }
    else
    {
        FlatUI::setProgressQss(bar, 20,12,16, "#747976", "#1ABC9C");
    }

}
use_percentage_bar::~use_percentage_bar()
{
    delete ui;
}
void use_percentage_bar::print_bar_8051(void)
{

    if(max_size==0)
    {
        qDebug()<<"erro:max=0";
        return;
    }
    uint8_t i;
    QString bar_str;
    qDebug()<<name<<":0";//<<QString().sprintf("%#x",base_addr);

    if(max_size/10.0<=1024)
    {
        bar_str+=QString().sprintf("%6.2f B %5s:|", max_size/10.0 , name.toStdString().data());
    }
    else
    {
        bar_str+=QString().sprintf("%6.2f KB %5s:|", max_size/10.0/1024.0 , name.toStdString().data());
    }

    for (i = 0; i < 20; i++)
    {
        if (i < (uint8_t)((used_size / 1.0) / (max_size / 1.0) * 20))
        {

            bar_str+=QString().sprintf("■");
        }
        else
        {
            bar_str+=QString().sprintf("_");
        }
    }



    if(max_size/10.0<=1024)
    {
        bar_str+=QString().sprintf("|%6.2f %% (%8.2f B / %8.2f B)", (used_size/10.0 / 1.0) / (max_size/10.0 / 1.0) * 100, used_size/10.0 , max_size/10.0);
    }
    else
    {
        bar_str+=QString().sprintf("|%6.2f %% (%8.2f B / %8.2f KB)", (used_size/10.0 / 1.0) / (max_size/10.0 / 1.0) * 100, used_size/10.0 , max_size/10.0 / 1024.0f);
    }



    bar_str+=QString().sprintf(" [%u B]", max_size/10.0 - used_size/10.0);
    qDebug()<< bar_str;

}
void use_percentage_bar::print_bar(void)
{

    if(max_size==0)
    {
        qDebug()<<"erro:max=0";
        return;
    }
    uint8_t i;
    QString bar_str;
    qDebug()<<name<<":"<<QString().sprintf("%#x",base_addr);
    bar_str+=QString().sprintf("%6.2f KB %5s:|", max_size / 1024.0f, storage==ram?"ram":"flash");



    for (i = 0; i < 20; i++)
    {
        if (i < (uint8_t)((used_size / 1.0) / (max_size / 1.0) * 20))
        {

            bar_str+=QString().sprintf("■");
        }
        else
        {
            bar_str+=QString().sprintf("_");
        }
    }



    if(max_size<=1024*1024)
    {
        bar_str+=QString().sprintf("|%6.2f %% (%8.2f KB / %8.2f KB)", (used_size / 1.0) / (max_size / 1.0) * 100, used_size / 1024.0f, max_size / 1024.0f);
    }
    else
    {
        bar_str+=QString().sprintf("|%6.2f %% (%8.4f MB / %8.4f MB)", (used_size / 1.0) / (max_size / 1.0) * 100, used_size / 1024.0f/1024.0f, max_size / 1024.0f/1024.0f);
    }



    bar_str+=QString().sprintf(" [%u B]", max_size - used_size);
    qDebug()<< bar_str;

}

QString use_percentage_bar::getName() const
{
    return name;
}

void use_percentage_bar::setName(const QString &value)
{
    name = value;


    ui->name->setTitle(name);
}

storage_type_t use_percentage_bar::getStorage() const
{
    return storage;
}

void use_percentage_bar::setStorage(const storage_type_t &value)
{
    storage = value;
}

uint64_t use_percentage_bar::getBase_addr() const
{
    return base_addr;
}

void use_percentage_bar::setBase_addr(const uint64_t &value)
{
    base_addr = value;
    ui->addr->setText(QString().sprintf("地址:%#x", base_addr));
}

uint64_t use_percentage_bar::getLoad_addr() const
{
    return load_addr;
}

void use_percentage_bar::setLoad_addr(const uint64_t &value)
{
    load_addr = value;
}

uint64_t use_percentage_bar::getLoad_max_size() const
{
    return load_max_size;
}

void use_percentage_bar::setLoad_max_size(const uint64_t &value)
{
    load_max_size = value;

}

uint64_t use_percentage_bar::getUsed_size() const
{
    return used_size;
}

void use_percentage_bar::setUsed_size(const uint64_t &value)
{
    used_size = value;
    if(max_size!=0)
    {
        ui->used_bar->setRange(0,max_size);
        ui->used_bar->setValue(used_size);

        if(storage==mc8051)
        {
            if(max_size/10.0<=1024)
            {
                ui->used->setText(QString().sprintf("占用:%.2f B / (%.2f B)",used_size/10.0,max_size/10.0));

            }
            else
            {
                ui->used->setText(QString().sprintf("占用:%.2f B / (%.2f KB)",used_size/10.0,max_size/10.0/1024.0));

            }
            setRemain_size(max_size-used_size);
        }else
        {
            if(max_size<=1024*1024)
            {
                ui->used->setText(QString().sprintf("占用:%.2f KB / (%.2f KB)",used_size/1024.0,max_size/1024.0));

            }
            else
            {
                ui->used->setText(QString().sprintf("占用:%.4f MB / (%.4f MB)",used_size/1024.0/1024.0,max_size/1024.0/1024.0));

            }
            setRemain_size(max_size-used_size);
        }
        set_bar_style(ui->used_bar,used_size/1.0/max_size/1.0);


    }
}

uint64_t use_percentage_bar::getMax_size() const
{
    return max_size;
}

void use_percentage_bar::setMax_size(const uint64_t &value)
{
    max_size = value;

    if(max_size!=0)
    {

        ui->used_bar->setRange(0,max_size);
        ui->used_bar->setValue(used_size);


        if(storage==mc8051)
        {


            if(max_size/10.0<=1024)
            {
                ui->used->setText(QString().sprintf("占用:%.2f B / (%.2f B)",used_size/10.0,max_size/10.0));

            }else
            {
                ui->used->setText(QString().sprintf("占用:%.2f B / (%.2f KB)",used_size/10.0,max_size/10.0/1024.0));

            }
            setRemain_size(max_size-used_size);
        }else
        {
            if(max_size<=1024*1024)
            {
                ui->used->setText(QString().sprintf("占用:%.2f KB / (%.2f KB)",used_size/1024.0,max_size/1024.0));

            }else
            {
                ui->used->setText(QString().sprintf("占用:%.4f MB / (%.4f MB)",used_size/1024.0/1024.0,max_size/1024.0/1024.0));

            }
            setRemain_size(max_size-used_size);
        }
        set_bar_style(ui->used_bar,used_size/1.0/max_size/1.0);


    }

}

uint64_t use_percentage_bar::getRemain_size() const
{
    return remain_size;
}

void use_percentage_bar::setRemain_size(const uint64_t &value)
{
    remain_size = value;
    if(storage==mc8051)
    {
        ui->remain->setText(QString().sprintf("剩余:%.2f B",remain_size/10.0));
    }else
    {
        ui->remain->setText(QString().sprintf("剩余:%llu B",remain_size));
    }
}

