# KDA v1.0 (keil_development_assistant)
keil开发助手项目

[简介](#1)<br>

[功能演示](#2)<br>

[开始使用](#3)<br>

[二次开发环境搭建](#4)<br>

[提供建议&&反馈bug](#5)<br>

<p id="1"></p>               

## 简介

这个项目的原型是就是[Keil5_disp_size_bar](https://gitee.com/nikolan/keil5_disp_size_bar)

C语言编写的一个解析map文件输出占用百分比进度条的命令行工具<br>
起初只是为了方便查看代码对芯片的内存ram和存储flash的占用<br>
项目推出不久收到不少网友的喜爱，提出了许多建议，使得项目得以完善。<br>

后来想更进一步开发更多功能，<br>
例如输出ram和flash的地址方便区分<br>
例如查看已使用的ram和flash里，每个文件的占比。<br>

到这里C语言和命令行也就不能很好的满足对这些功能的开发了<br>
于是用Qt重新开发了一下，于是推出升级版的KDA(keil_development_assistant)<br>
keil开发助手项目就这样诞生了。

注意！安装前请先备份系统的PATH环境变量！<br>
注意！安装前请先备份系统的PATH环境变量！<br>
注意！安装前请先备份系统的PATH环境变量！<br>

先保存自己的系统 PATH 和 用户 PATH。<br>
部分网友反馈工具自动添加到PATH有概率导致原有的PATH被清空！<br>
可能和系统版本有关，打包脚本写的不严谨。<br>

注意！程序显示的最高主频是错的，应该是默认内部时钟<br>
注意！程序显示的最高主频是错的，应该是默认内部时钟<br>
注意！程序显示的最高主频是错的，应该是默认内部时钟<br>
后面有更新再一起更改。<br>


<p id="2"></p> 

## 功能演示
1.对stm32或者各种国产32工程解析。<br>

![stm32F1_1](https://gitee.com/nikolan/pic-bed/raw/master/stm32F1_1.png)<br>
![stm32F1_2](https://gitee.com/nikolan/pic-bed/raw/master/stm32F1_2.png)<br>
![stm32F1_3](https://gitee.com/nikolan/pic-bed/raw/master/stm32F1_3.png)<br>
![At32F4_1](https://gitee.com/nikolan/pic-bed/raw/master/AT32F4_1.png)<br>
![At32F4_2](https://gitee.com/nikolan/pic-bed/raw/master/AT32F4_2.png)<br>
![At32F4_3](https://gitee.com/nikolan/pic-bed/raw/master/AT32F4_3.png)<br>

3.对8051单片机的工程解析。<br>
![8051_1](https://gitee.com/nikolan/pic-bed/raw/master/8051_1.png)<br>
![8051_2](https://gitee.com/nikolan/pic-bed/raw/master/8051_2.png)<br>


<p id="3"></p> 

## 开始使用

1. 安装KDA<br>
下载代码仓库右侧的发行版<br>
![down_1](https://gitee.com/nikolan/pic-bed/raw/master/down_1.png)<br>
![down_2](https://gitee.com/nikolan/pic-bed/raw/master/down_2.png)<br>
2. 勾选协议<br>
![install_1](https://gitee.com/nikolan/pic-bed/raw/master/KDA_install_1.png)<br>
3. 选择安装路径<br>
![install_2](https://gitee.com/nikolan/pic-bed/raw/master/KDA_install_2.png)<br>
4. 安装完成<br>
![install_3](https://gitee.com/nikolan/pic-bed/raw/master/KDA_install_3.png)<br>
 普通用法<br>
5. 打开KDA选择工程目录，会递归查找子目录，<br>
   选择工程目录后会自动选map目录为工程目录<br>
   如果map和工程不在一个命命令再选map所在目录<br>
   选择手动更新<br>
![use_3](https://gitee.com/nikolan/pic-bed/raw/master/KDA_use3.png)<br>


- 进阶用法<br>
软件打包时已经设置添加软件安装路径到系统环境变量Path<br>
如果并没有添加成功则需要自己手动添加<br>
![intsall_4](https://gitee.com/nikolan/pic-bed/raw/master/KDA_install_4.png)<br>

在keil 打开魔术棒选User在after bulid的Run #1或Run #2填入<br>
```
KDA "$P"
``` 
<br>
然后勾选运行<br>

![use_1](https://gitee.com/nikolan/pic-bed/raw/master/KDA_use1.png)<br>

在每次编译成功后就会弹出KDA的窗口<br>

![use_2](https://gitee.com/nikolan/pic-bed/raw/master/KDA_use2.png)<br>
<p id="4"></p> 


## 二次开发环境搭建

开发环境:<br>
- 系统:window10 x64位 <br>
- 框架:Qt5.12.9<br>
- IDE:Qt Creator 4.12.2 (Community)<br>
- 编译链:mingw64<br>

Qt项目库依赖:<br>

```
 QT       += core gui xml charts  

``` 

克隆仓库代码:<br>

``` shell

git clone https://gitee.com/nikolan/keil_development_assistant.git

```

使用Qt Creator打开项目即可开始二次开发<br>

<p id="5"></p> 

## 提供建议&&反馈bug

可以在代码仓库提Issues<br>

[issues](https://gitee.com/nikolan/keil_development_assistant/issues)


或者发送到邮箱2503865771@qq.com提醒我处理<br>


<p id="6"></p> 

## 参与奉献&&赞助






