#include "only_run_by_file.h"

only_run_by_file::only_run_by_file(QObject *parent) : QObject(parent),
    runFileName("run"),
    closeFileName("close")
{



    // 如果 run 文件已经存在，则删除
    if (QFile::exists(runFileName)) {
        QFile::remove(runFileName);

        // 检测 close 文件是否存在的定时器
        closeTimer = new QTimer(this);
        connect(closeTimer, &QTimer::timeout, this, &only_run_by_file::checkCloseFile);
        closeTimer->start(300); // 每 300 毫秒执行一次检测
    }else
        createRunFile();




    // 检测 run 文件是否存在的定时器
    timer = new QTimer(this);
    connect(timer, &QTimer::timeout, this, &only_run_by_file::checkRunFile);
    timer->start(800); // 每 800 毫秒执行一次检测
}

void only_run_by_file::createCloseFile()
{
    // 创建 close 文件
    QFile file(closeFileName);
    file.open(QIODevice::WriteOnly);
    file.close();
}

void only_run_by_file::createRunFile()
{
    // 创建 run 文件
    QFile file(runFileName);
    file.open(QIODevice::WriteOnly);
    file.close();
}

void only_run_by_file::deleteCloseFile()
{
    // 如果 close 文件已经存在，则删除
    if (QFile::exists(closeFileName)) {
        QFile::remove(closeFileName);
    }

}
void only_run_by_file::deleteRunFile()
{
    // 如果 run 文件已经存在，则删除
    if (QFile::exists(runFileName)) {
        QFile::remove(runFileName);
    }
}



void only_run_by_file::checkRunFile()
{
    // 检测 run 文件是否存在
    if (!QFile::exists(runFileName)) {
        // 文件不存在，采集关闭操作并创建 close 文件
        deleteCloseFile();
        // 创建 close 文件
        createCloseFile();
        emit runFileDeleted();
    }
}

void only_run_by_file::checkCloseFile()
{
    static uint8_t cnt=0;

    if(cnt<3)
    {

        cnt++;
        // 检测 close 文件是否存在
        if (QFile::exists(closeFileName)) {
            // 发出信号，通知检测到 close 文件
            closeTimer->stop();
            // 如果 close 文件已经存在，则删除
            deleteCloseFile();
            // 创建 run 文件
            createRunFile();



            emit closeFileDetected();


        }

    }else
    {
        cnt=0;
        // 如果 close 文件已经存在，则删除
        deleteCloseFile();
        // 创建 run 文件
        createRunFile();
        emit closeFileDetected();
    }

}
