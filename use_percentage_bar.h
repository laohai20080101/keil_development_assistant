#ifndef USE_PERCENTAGE_BAR_H
#define USE_PERCENTAGE_BAR_H

#include <QWidget>
#include <QProgressBar>
namespace Ui {
class use_percentage_bar;
}
typedef enum storage_type {ram,flash,mc8051 }storage_type_t;

class use_percentage_bar : public QWidget
{
    Q_OBJECT

public:
    explicit use_percentage_bar(QWidget *parent = nullptr);
    ~use_percentage_bar();


    void print_bar(void);
    void print_bar_8051(void);

    QString getName() const;
    void setName(const QString &value);

    storage_type_t getStorage() const;
    void setStorage(const storage_type_t &value);

    uint64_t getBase_addr() const;
    void setBase_addr(const uint64_t &value);

    uint64_t getLoad_addr() const;
    void setLoad_addr(const uint64_t &value);

    uint64_t getLoad_max_size() const;
    void setLoad_max_size(const uint64_t &value);

    uint64_t getUsed_size() const;
    void setUsed_size(const uint64_t &value);

    uint64_t getMax_size() const;
    void setMax_size(const uint64_t &value);

    uint64_t getRemain_size() const;
    void setRemain_size(const uint64_t &value);

private:
    Ui::use_percentage_bar *ui;

    QString name;
    storage_type_t storage;
    uint64_t base_addr;
    uint64_t load_addr;
    uint64_t load_max_size;
    uint64_t used_size;
    uint64_t max_size;
    uint64_t remain_size;
    void set_bar_style(QProgressBar *bar,float per);
};

#endif // USE_PERCENTAGE_BAR_H
