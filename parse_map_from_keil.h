#ifndef PARSE_MAP_FROM_KEIL_H
#define PARSE_MAP_FROM_KEIL_H

#include <QString>
#include "use_percentage_bar.h"
#include <QList>


typedef struct {QString fn_name;QString file_name; uint64_t size;}map_item;


typedef struct image_item {

    QString Code;
    QString RO_Data;
    QString RW_Data;
    QString ZI_Data;
    QString Debug;
    QString Object_Name;

}image_item_t;

class parse_map_from_keil
{

public:
    bool to_parse_map_from_keil(QString path);
    parse_map_from_keil(QString path,
                        uint64_t to_iram_base,
                        uint64_t to_iram_max_size,
                        uint64_t to_irom_base,
                        uint64_t to_irom_max_size
                        );
    QString find_map_path(const QString& dir_path);
    bool parse_excution_region_info(QString info_string);
    bool parse_map_file(QString path);


    QList<use_percentage_bar*>ram_list;
    QList<use_percentage_bar*>flash_list;


    QString getMap_path() const;
    void setMap_path(const QString &value);

    QList<image_item_t*> getInfo_list() const;
    void setInfo_list(const QList<image_item_t*> &value);

    uint64_t getAll_ram_size() const;

    uint64_t getAll_flash_size() const;

private:


    uint64_t iram_base;
    uint64_t iram_max_size;

    uint64_t irom_base;
    uint64_t irom_max_size;



    uint64_t all_ram_size=0;
    uint64_t all_flash_size=0;
    //,"HEAP","STACK","heap","stack"
    QList<QString> ram_keyword={"ER$$","RW_","ER_RW","ER_ZI"};
    QList<QString> flash_keyword={"RO","FLASH","flash","ro","text","TEXT"};

    QString map_path;
    QList<image_item_t*> info_list;
};

#endif // PARSE_MAP_FROM_KEIL_H
