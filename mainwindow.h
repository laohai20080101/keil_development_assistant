﻿#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "framelessmainwindow.h"

#include "parse_keil_project.h"
#include "parse_map_from_keil.h"
#include "parse_m51_from_keil.h"

#include <QtCharts>
extern QString project_path;
extern QString map_path;
extern bool only_printf;

QT_CHARTS_USE_NAMESPACE
namespace Ui {
class MainWindow;
}
typedef enum{ram_table,flash_table}ui_table_t;
class MainWindow : public FramelessMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void winow_on_top(bool is_top);
    void setPieSliceStyle(QPieSlice* slice, qreal percentage, qreal maxPercentage);
    void set_bar_style(QProgressBar *bar,float per);
    void update_cpu_info(void);
    void update_map_info(void);
    void update_m51_info(void);
    QString readTableData(ui_table_t table,int row, int column);
    void writeTableData(ui_table_t table,int row, int column, const QString &data);
    void map_calculate_size_by_address(parse_map_from_keil* map);
    Ui::MainWindow *ui;

    QPieSeries *ram_pieseries=nullptr;
    QPieSeries *flash_pieseries=nullptr;
    QChart* ram_chart=nullptr;
    QChart* flash_chart=nullptr;


private:


    void clearLayout(QLayout* layout);
    parse_keil_project* cpu=nullptr;
    parse_map_from_keil* map=nullptr;
    parse_m51_from_keil* m51=nullptr;

private slots:
    void initForm();
    void titleDblClick();
    void windowStateChange(bool max);

private slots:
    void on_btnMenu_Min_clicked();
    void on_btnMenu_Max_clicked();
    void on_btnMenu_Close_clicked();
    void on_btn_set_project_path_clicked();
    void on_btn_set_map_path_clicked();
    void on_update_info_clicked();
    void on_ram_table_itemSelectionChanged();
    void on_flash_table_itemSelectionChanged();
    void on_gitee_index_clicked();
    void on_icon_clicked();
};
extern MainWindow* main_window;
#endif // MAINWINDOW_H
